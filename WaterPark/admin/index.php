<?php
    session_start();
    require_once("dbconfig/dbconfig.php"); 
    require_once("action/logout.php");
    if(!isset($_SESSION['key'])){
        header('Location: admin-frm/frm-login.php');
    }
    if(isset($_GET['cate'])){
        $cate = $_GET['cate'];
    }
    
    else $cate = "Home";
    $h = file_get_contents('web-part/nav-bar.html');
    $h = str_replace('{{title}}',$cate, $h);
    $pattern = '/\{!.+!\} /i';
    $h = str_replace("{!$cate!} ","class=actived ", $h);
    $h = preg_replace("$pattern","", $h);
    echo $h;

?>
<main>
    <div class="info-bar">
        <div class="info-bar-text">
            <p style="font-size:16px">Welcom <?= $_SESSION['user']; ?>, </p>
            <a href="?exec=logout" class="logout">Logout</a>
        </div>
        <div class="cate">
            <p><?=$cate; ?></p>
        </div>
    </div>
    <div class="main">
        <?php 
            require_once("action/get-data.php");    
        ?>
</main>
<?php
    require_once("web-part/footer.html");
?>
