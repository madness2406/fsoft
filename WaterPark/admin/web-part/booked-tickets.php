<?php
    require_once('dbconfig/dbconfig.php');

    $color = 1;
    $sql =  "SELECT * FROM booked_tickets";
    $result = mysqli_query($conn,$sql);
    $data = mysqli_fetch_all($result,1);
    ?>
    <div class="finding">
        <input type="text" id="search-data">
        <button id="search" class="btn">Search</button>
    </div>
    <div class=work>
            <table>
                <thead>
                    <tr>
                        <th>Custom Name</th>
                        <th>Phone</th>
                        <th>Person ID</th>
                        <th>Email</th>
                        <th>Adult</th>
                        <th>Child</th>
                        <th>Family</th>
                        <th>Booked Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id=response>
                    <?php
                            for($i = 0; $i < count($data); $i++){
                                $color = -$color;
                                if($color == -1) echo "<tr class=stripe>";
                                else echo "<tr>";
                                echo "<td>" . $data[$i]['cus_name'] . "</td>";
                                echo "<td>" . $data[$i]['phone'] . "</td>";
                                echo "<td>" . $data[$i]['person_id'] . "</td>";
                                echo "<td>" . $data[$i]['email'] . "</td>";
                                echo "<td>" . $data[$i]['adult'] . "</td>";
                                echo "<td>" . $data[$i]['child'] . "</td>";
                                echo "<td>" . $data[$i]['family'] . "</td>";
                                echo "<td>" . $data[$i]['booked_time'] . "</td>";
                                echo "<td class=action><button class=work_del style='margin-left:25%;' onclick=delete2(" . $data[$i]['id'] . ")>Del</button>"."</td>";
                                echo '</tr>';
                        }
                    ?>
                </tbody>
            </table>
    </div>