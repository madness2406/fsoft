<?php
    require_once('dbconfig/dbconfig.php');

    $color = 1;
    $sql =  "SELECT * FROM news ORDER BY time ASC";
    $result = mysqli_query($conn,$sql);
?>
<button class="btn addnew" onclick="location.href = 'admin-frm/frm-post-news.php'">Add News</button>
<div class="showdata">
    <select name="category" id="show-data">
        <option value="All">All</option>
        <option value="Park Infomation">Park Infomation</option>
        <option value="Gallery">Gallery</option>
        <option value="Food zone">Food zone</option>
        <option value="Facilities">Facilities</option>
    </select>
</div>
<div class=work>
            <table>
                <thead>
                    <tr>
                        <th class="th-pic">Picture</th>
                        <th class="th-cat">Category</th>
                        <th class="th-tit">Title</th>
                        <th class="th-time">Upload Time
                            <div class="soft">
                                <i id="soft-desc" class="arrow up"></i>
                                <i id="soft-asc" class="arrow down"></i>
                            </div>
                        </th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id=response>
                    <?php
                        while ($data = mysqli_fetch_object($result)) {
                            $color = -$color;
                            if($color == -1) echo "<tr class=stripe>";
                            else echo "<tr>";
                            if($data->picture == NULL) echo "<td>No picture</td>";
                            else {
                                if($data->category === "Facilities") echo "<td><img src='images/Facilities/" . $data->picture . "'>";
                                if($data->category === "Park Infomation") echo "<td><img src='images/ParkInfomation/" . $data->picture . "'>";
                                if($data->category === "Food zone") echo "<td><img src='images/Foodzone/" . $data->picture . "'>";
                                if($data->category === "Gallery") echo "<td><img src='images/Gallery/" . $data->picture . "'>";
                            }
                            echo "<td>".$data->category."</td>";
                            echo "<td>".$data->title."</td>";
                            echo "<td style='font-size: 13px;text-align:center;'>".$data->time."</td>";
                            echo "<td class=action>"."<a href='admin-frm/frm-edit-news.php?id=$data->id'><button class=work_edit>Edit</button></a>";
                            echo "<button class=work_del onclick=delete1(" . $data->id . ")>Del</button>"."</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
    </div>