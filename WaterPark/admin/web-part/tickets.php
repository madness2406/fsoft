<?php
    require_once('dbconfig/dbconfig.php');

    $color = 1;
    $sql =  "SELECT * FROM ticket";
    $result = mysqli_query($conn,$sql);
    $data = mysqli_fetch_all($result,1);
    ?>
    <div class=work>
            <table>
                <thead>
                    <tr>
                        <th>Thumbnail</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                            for($i = 0; $i < count($data); $i++){
                                $color = -$color;
                                if($color == -1) echo "<tr class=stripe>";
                                else echo "<tr>";
                                if($data[$i]['Thumbnail'] == NULL) echo "<td>No picture</td>";
                                else echo "<td><img src='images/ticket/" . $data[$i]['Thumbnail'] . "'>";
                                echo "<td>" . $data[$i]['Name'] . "</td>";
                                echo "<td>" . $data[$i]['Description'] . "</td>";
                                echo "<td>" . $data[$i]['Price'] . "</td>";
                                echo "<td class=action>"."<a style='margin-left:25%;' href='admin-frm/frm-edit-ticket.php?id=" . $data[$i]['Id'] . "'><button class=work_edit>Edit</button></a>";
                                echo '<tr>';
                        }
                    ?>
                </tbody>
            </table>
    </div>