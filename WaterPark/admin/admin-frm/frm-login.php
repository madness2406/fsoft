<?php
    require_once("../dbconfig/dbconfig.php"); 
    require_once("../action/login.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Login</title>
</head>
<body>
    <div class="container">
        <form action="frm-login.php" method="post">
            <h3>Log in</h3>
            <div class="inputBox">
                <span>Username</span>
                <div class="box">
                    <div class="icon"><ion-icon name="person"></ion-icon></div>
                    <input type="text" name="user" id="">
                </div>
            </div>
            <div class="inputBox">
                <span>Password</span>
                <div class="box">
                    <div class="icon"><ion-icon name="lock-closed"></ion-icon></div>
                    <input type="password" name="password" id="">
                </div>
            </div>
            <div class="inputBox">
                <div class="box">
                    <input type="submit" value="Log in" name=btn-login>
                </div>
            </div>
        </form>
    </div>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</body>
</html>