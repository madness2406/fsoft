<?php
    require_once("../dbconfig/dbconfig.php");
    $txt_id = $_GET['id'];
	$sql = "SELECT * FROM news WHERE id=$txt_id";
	$query = mysqli_query($conn,$sql);
	$data = mysqli_fetch_assoc($query);
        $id = $data['id'];
		$txt_title = $data['title'];
		$txt_content = $data['content'];
        $txt_image = $data['picture'];
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Edit News</title>
</head>
<body>
    <div class="container news">
        <form action="../action/edit-news.php" method="post" enctype="multipart/form-data">
            <h3>Edit News</h3>
            <div class="inputBox">
                <textarea name="id" style="display:none;"><?php echo $id ?></textarea>
                <span>Title</span>
                <div class="box">
                    <textarea name="title" id="" class="title"><?php echo $txt_title ?></textarea>
                </div>
            </div>
            <div class="inputBox">
                <span>Category</span>
                <div class="box">
                    <select name="category">
                        <option value=""></option>
                        <option value="Park Infomation">Park Infomation</option>
                        <option value="Gallery">Gallery</option>
                        <option value="Food zone">Food zone</option>
                        <option value="Facilities">Facilities</option>
                    </select>
                </div>
            </div>
            <span class=name-span>Content</span>
            <div class="box">
                <textarea name="content" id="editor"><?php echo $txt_content ?></textarea>
            </div>
            <span class=name-span>Title image</span>
            <div class="box">
                <input type="file" name=image accept="image/*">
            </div>
            <div class="inputBox">
                <div class="box">
                    <input type="submit" value="Update" name=btn-update>
                </div>
            </div>
        </form>
    </div>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <script src="../ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editor',{
            filebrowserUploadUrl: '../ckeditor/upload.php',
            filebrowserUploadMethod: 'form'
        });
    </script>
</body>
</html>