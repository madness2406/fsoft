<?php
    require_once("../dbconfig/dbconfig.php");
    $txt_id = $_GET['id'];
	$sql = "SELECT * FROM ticket WHERE id=$txt_id";
	$query = mysqli_query($conn,$sql);
	$data = mysqli_fetch_assoc($query);
        $id = $data['Id'];
		$txt_name = $data['Name'];
		$txt_description = $data['Description'];
        $txt_thumbnail = $data['Thumbnail'];
        $txt_price = $data['Price'];
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Edit Ticket</title>
</head>
<body>
    <div class="container news">
        <form action="../action/edit-ticket.php" method="post" enctype="multipart/form-data">
            <h3>Edit Ticket</h3>
            <div class="inputBox">
                <textarea name="id" style="display:none;"><?= $id ?></textarea>
                <span>Name</span>
                <div class="box">
                    <textarea name="name" id="" class="name"><?= $txt_name ?></textarea>
                </div>
            </div>
            <div class="inputBox">
                <span class=name-span>Description</span>
                <div class="box">
                    <textarea name="description" id=""><?= $txt_description ?></textarea>
                </div>
                <span>Price</span>
                <div class="box">
                    <input type="number" name="price" id="" min="0" value="<?= $txt_price ?>">
                </div>
                <span class=name-span>Thumbnail</span>
                <div class="box">
                    <input type="file" name=image accept="image/*">
                </div>
                <div class="inputBox">
                    <div class="box">
                        <input type="submit" value="Update" name=btn-update>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</body>
</html>