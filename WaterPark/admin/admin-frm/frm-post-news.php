<?php
    require_once("../dbconfig/dbconfig.php"); 
    require_once("../action/upload.php");
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Post News</title>
</head>
<body>
    <div class="container news">
        <form action="frm-post-news.php" method="post" enctype="multipart/form-data">
            <h3>Add News</h3>
            <div class="inputBox">
                <span class=name-span>Title</span>
                <div class="box">
                    <textarea name="title" id="" class="title"></textarea>
                </div>
            </div>
            <div class="inputBox">
                <span>Category</span>
                <div class="box">
                    <select name="category">
                        <option value=""></option>
                        <option value="Park Infomation">Park Infomation</option>
                        <option value="Gallery">Gallery</option>
                        <option value="Food zone">Food zone</option>
                        <option value="Facilities">Facilities</option>
                    </select>
                </div>
            </div>
                <span  class=name-span>Content</span>
                <textarea name="content" id="editor"></textarea>
            <span  class=name-span>Title Image</span>
            <div class="box">
                <input type="file" name=image accept="image/*">
            </div>
            <div class="inputBox">
                <div class="box">
                    <input type="submit" value="Publish" name=btn-publish>
                </div>
            </div>
        </form>
    </div>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    
    <!-- CK4 -->
    <script src="../ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editor',{
            filebrowserUploadUrl: '../ckeditor/upload.php',
            filebrowserUploadMethod: 'form'
        });
    </script>
    <!-- End CK4 -->

</body>
</html>