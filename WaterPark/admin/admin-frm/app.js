
function delete1(id){
    if(confirm("Do you really want to delete it?") == true){
        $.ajax({
            url: 'action/delete-news.php?id=' + id,
            type: 'POST',
            success:function(data){
                $('#response').html(data);
            }
        });
    }
};
function delete2(id){
    if(confirm("Do you really want to delete it?") == true){
        $.ajax({
            url: 'action/delete-booked.php?id=' + id,
            type: 'POST',
            success:function(data){
                $('#response1').html(data);
            }
        });
    }
};
function logout1() {
    if(confirm("Do you want to logout?") == true){
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "action/logout.php", true);
        xhttp.send();
    }
}
$(document).ready(function() {
    $("#show-data").change(function(){
        $.ajax({
            url: 'ajax/ajax-display.php?cate=' + $('#show-data option:selected').val() + '&page=1',
            type: 'POST',
            success:function(data){
                $('#response').html(data);
            }
        });
    });
});
$('#soft-asc').click(function(){
    $.ajax({
        url: 'ajax/ajax-display.php?soft=1&cate=' + $('#show-data option:selected').val() + '&page=1',
        type: 'POST',
        success:function(data){
            $('#response').html(data);
        }
    });
});
$('#soft-desc').click(function(){
    $.ajax({
        url: 'ajax/ajax-display.php?soft=2&cate=' + $('#show-data option:selected').val() + '&page=1',
        type: 'POST',
        success:function(data){
            $('#response').html(data);
        }
    });
});
$('#search').click(function(){
    $.ajax({
        url: 'ajax/ajax-search.php?search=' + $('#search-data').val(),
        type: 'POST',
        success:function(data){
            $('#response').html(data);
        }
    });
});
