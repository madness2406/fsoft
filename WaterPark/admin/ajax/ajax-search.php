<?php

    require_once('../dbconfig/dbconfig.php');
    if(isset($_GET['search'])){
        $sql =  "SELECT * FROM booked_tickets WHERE cus_name LIKE '%".$_GET['search']."%'";
        $query = mysqli_query($conn,$sql);
        if(mysqli_num_rows($query) == 0){
            echo "<tr><td colspan=9>No data</td></tr>";
        }
        else{
            $color = 1;
            while ($data = mysqli_fetch_object($query)) {
                $color = -$color;
                if($color == -1) echo "<tr class=stripe>";
                else echo "<tr>";
                echo "<td>".$data->cus_name."</td>";
                echo "<td>".$data->phone."</td>";
                echo "<td>".$data->person_id."</td>";
                echo "<td>".$data->email."</td>";
                echo "<td>".$data->adult."</td>";
                echo "<td>".$data->child."</td>";
                echo "<td>".$data->family."</td>";
                echo "<td>".$data->booked_time."</td>";
                echo "<button class=work_del onclick=delete1(" . $data->id . ")>Del</button>"."</td>";
                echo "</tr>";
            }
        }
    }