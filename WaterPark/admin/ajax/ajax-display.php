<?php
    require_once('../dbconfig/dbconfig.php');
    require_once('../action/pagination.php');

   if(isset($_GET['cate'])){
        if(isset($_GET['soft'])){
            if($_GET['soft'] == 1){
                if($_GET['cate'] == "All"){
                $sql =  "SELECT * FROM news ORDER BY time ASC";
                }
                else{
                    $sql =  "SELECT * FROM news WHERE category = '" . $_GET['cate'] . "' ORDER BY time ASC";
                }
            }
            else{
                if($_GET['cate'] == "All"){
                    $sql =  "SELECT * FROM news ORDER BY time DESC";
                    }
                    else{
                        $sql =  "SELECT * FROM news WHERE category = '" . $_GET['cate'] . "' ORDER BY time DESC";
                    }
            }
        }
        else{
            if($_GET['cate'] == "All"){
                $sql =  "SELECT * FROM news ORDER BY time ASC";
            }
            else{
                $sql =  "SELECT * FROM news WHERE category = '" . $_GET['cate'] . "' ORDER BY time ASC";
            }
        }
   }

    $query = mysqli_query($conn,$sql);
    if(mysqli_num_rows($query) == 0){
        echo "<tr><td colspan=5>No data</td></tr>";
    }
    else{
        $color = 1;
        while ($data = mysqli_fetch_object($query)) {
            $color = -$color;
            if($color == -1) echo "<tr class=stripe>";
            else echo "<tr>";
            if($data->picture == NULL) echo "<td>No picture</td>";
            else {
                if($data->category === "Facilities") echo "<td><img src='images/Facilities/" . $data->picture . "'>";
                if($data->category === "Park Infomation") echo "<td><img src='images/ParkInfomation/" . $data->picture . "'>";
                if($data->category === "Food zone") echo "<td><img src='images/Foodzone/" . $data->picture . "'>";
                if($data->category === "Gallery") echo "<td><img src='images/Gallery/" . $data->picture . "'>";
            }
            echo "<td>".$data->category."</td>";
            echo "<td>".$data->title."</td>";
            echo "<td style='font-size: 13px;text-align:center;'>".$data->time."</td>";
            echo "<td class=action>"."<a href='frm-edit-news.php?id=$data->id'><button class=work_edit>Edit</button></a>";
            echo "<button class=work_del onclick=delete1(" . $data->id . ")>Del</button>"."</td>";
            echo "</tr>";
        }
    }
    ?>