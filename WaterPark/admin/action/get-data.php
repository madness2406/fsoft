<?php
    if(isset($_GET['cate'])){
        $cate = $_GET['cate'];
        if($cate == 'News'){
            require_once("web-part/news.php");
        }
        if($cate == 'Booked Tickets'){
            require_once("web-part/booked-tickets.php");
        }
        if($cate == 'Tickets'){
            require_once("web-part/tickets.php");
        }
    }
    else {
        $cate = "Home";

        $sql =  "SELECT COUNT(*) AS Total FROM news";
        $result = mysqli_query($conn,$sql);
        $total = mysqli_fetch_object($result);
        ?>
        <link rel="stylesheet" href="style1.css">
        <div class="container-home">
            <a href=?cate=News class="box-home" style="--clr:#fc5f9b;">
                <div class="content-home">
                    <div class="icon-home"><ion-icon name="newspaper-outline"></ion-icon></div>
                    <div class="text-home">
                        <h3>News</h3>
                        <p><?php echo $total->Total; ?></p>
                    </div>
                </div>
            </a>
        <?php
        $sql =  "SELECT COUNT(*) AS Total FROM booked_tickets";
        $result = mysqli_query($conn,$sql);
        $total = mysqli_fetch_object($result);
        ?>
            <a href="?cate=Booked Tickets" class="box-home" style="--clr:#a362ea;">
                <div class="content-home">
                    <div class="icon-home"><ion-icon name="ticket-outline"></ion-icon></div>
                    <div class="text-home">
                        <h3>Booked Tickets</h3>
                        <p><?php echo $total->Total; ?></p>
                    </div>
                </div>
            </a>
        <?php
        $sql =  "SELECT COUNT(*) AS Total FROM ticket";
        $result = mysqli_query($conn,$sql);
        $total = mysqli_fetch_object($result);
    ?>
            <a href=?cate=Tickets class="box-home" style="--clr:#0ed095;">
                <div class="content-home">
                    <div class="icon-home"><ion-icon name="people-outline"></ion-icon></div>
                    <div class="text-home">
                        <h3>Tickets</h3>
                        <p><?php echo $total->Total; ?></p>
                    </div>
                </div>
            </a>
        </div>
<?php } ?>
