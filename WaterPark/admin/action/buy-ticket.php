<?php

require_once("../dbconfig/dbconfig.php");
  
  // If upload button is clicked ...
            $cus_name = $_POST['name'];
            $phone = $_POST['phone_number'];
            $per_id = $_POST['personal_id'];
            $email = $_POST['email'];
            $adult = $_POST['t0'];
            $child = $_POST['t1'];
            $family = $_POST['t2'];
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                ?>
                <script>
                    alert("Invalid email, pls try again");
                    window.location.href = "../../index.php#buy_ticket";
                </script>
                <?php
                die();
            }
            if(!preg_match('/^[0-9]{10}+$/', $phone)){
                ?>
                <script>
                    alert("Invalid phone, pls try again");
                    window.location.href = "../../index.php#buy_ticket";
                </script>
                <?php
                die();
            }
            if($adult == 0 && $child == 0 && $family == 0){
                ?>
                <script>
                    alert("Please select at least one ticket");
                    window.location.href = "../../index.php#buy_ticket";
                </script>
                <?php
                die();
            }
            // Get all the submitted data from the form
            $sql = "INSERT INTO booked_tickets (cus_name,phone,person_id,email,adult,child,family) VALUES ('$cus_name','$phone','$per_id','$email','$adult','$child','$family')";
            // Execute query
            mysqli_query($conn, $sql);
            ?>
                <script>
                    alert("Thank you for booking ticket. See you soon.")
                    window.location.href = "../../index.php";
                </script>