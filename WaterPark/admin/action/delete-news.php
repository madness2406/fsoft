<?php
    error_reporting(0);
    require_once("../dbconfig/dbconfig.php");
    $folder = "";
    $sql =  "SELECT * FROM news WHERE id= '" . $_GET['id'] . "'";
    $result = mysqli_query($conn,$sql);
    $data = mysqli_fetch_all($result,1);
    if($data[0]['category'] === "Facilities") $folder = "../images/Facilities/";
    if($data[0]['category'] === "Park Infomation") $folder = "../images/ParkInfomation/";
    if($data[0]['category'] === "Food zone") $folder = "../images/Foodzone/";
    if($data[0]['category'] === "Gallery") $folder = "../images/Gallery/";
    if($data[0]['picture'] !== ""){
        $del_file = $folder . $data[0]['picture'];
        if (file_exists($del_file)){
            unlink($del_file);
        }
    }

    //Delete image in content
    $pattern="/\.\.\/images\/uploads\/.+\" s/";
    preg_match_all($pattern, $data[0]['content'], $arr);
    if($arr[0] !== NULL){
        for($i=0;$i<count($arr[0]);$i++){
            $arr[0][$i] = str_replace("\" s","",$arr[0][$i]);
            $del_file =  $arr[0][$i];
            unlink($del_file);
        }
    }
    
    $sql = "DELETE FROM news WHERE id = '" . $_GET['id'] . "'";
    mysqli_query($conn,$sql);

    $sql =  "SELECT * FROM news ORDER BY time ASC";
    $query = mysqli_query($conn,$sql);
    if(mysqli_num_rows($query) == 0){
        echo "<tr><td colspan=5>No data</td></tr>";
    }
    else{
        $color = 1;
        while ($data = mysqli_fetch_object($query)) {
            $color = -$color;
            if($color == -1) echo "<tr class=stripe>";
            else echo "<tr>";
            if($data->picture == NULL) echo "<td>No picture</td>";
            else {
                if($data->category === "Facilities") echo "<td><img src='images/Facilities/" . $data->picture . "'>";
                if($data->category === "Park Infomation") echo "<td><img src='images/ParkInfomation/" . $data->picture . "'>";
                if($data->category === "Food zone") echo "<td><img src='images/Foodzone/" . $data->picture . "'>";
                if($data->category === "Gallery") echo "<td><img src='images/Gallery/" . $data->picture . "'>";
            }
            echo "<td>".$data->category."</td>";
            echo "<td>".$data->title."</td>";
            echo "<td style='font-size: 13px;text-align:center;'>".$data->time."</td>";
            echo "<td class=action>"."<a href='frm-edit-news.php?id=$data->id'><button class=work_edit>Edit</button></a>";
            echo "<button class=work_del onclick=delete1(" . $data->id . ")>Del</button>"."</td>";
            echo "</tr>";
        }
    }