<?php
    $result = mysqli_query($conn, 'select count(id) as total from news');
    $row = mysqli_fetch_assoc($result);
    $total_records = $row['total'];
     
    $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
    $limit = 5;
     
    $total_page = ceil($total_records / $limit);
     
    if ($current_page > $total_page){
        $current_page = $total_page;
    }
    else if ($current_page < 1){
        $current_page = 1;
    }
     
    $start = ($current_page - 1) * $limit;
?>
    