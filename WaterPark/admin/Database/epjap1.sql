-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2022 at 05:36 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epjap1`
--
CREATE DATABASE IF NOT EXISTS `epjap1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `epjap1`;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `user` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `user`, `password`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997');

-- --------------------------------------------------------

--
-- Table structure for table `booked_tickets`
--

CREATE TABLE `booked_tickets` (
  `id` int(11) NOT NULL,
  `cus_name` varchar(40) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `person_id` varchar(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `adult` int(11) NOT NULL,
  `child` int(11) NOT NULL,
  `family` int(11) NOT NULL,
  `booked_time` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booked_tickets`
--

INSERT INTO `booked_tickets` (`id`, `cus_name`, `phone`, `person_id`, `email`, `adult`, `child`, `family`, `booked_time`) VALUES
(1, 'test', '123456789', '123456789', 'test@test.com', 1, 1, 1, '2022-03-07 15:48:24');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `category` text NOT NULL,
  `content` varchar(4000) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `time` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `category`, `content`, `picture`, `time`) VALUES
(1, '4 seasons swimming pool', 'Facilities', '<p>4 seasons swimming pool</p>\r\n', 'beboi.jpg', '2022-03-07 20:40:50'),
(2, 'Swimming pool rescue', 'Facilities', '<p>Swimming pool rescue</p>\r\n', 'cuuho.jpg', '2022-03-07 20:42:16'),
(3, '4 star canteen', 'Facilities', '<p>4 star canteen</p>\r\n', 'cangtin.png', '2022-03-07 20:42:35'),
(4, 'Sauna room', 'Facilities', '<p>Sauna room</p>\r\n', 'xonghoi.jpg', '2022-03-07 20:43:18'),
(5, 'Adventure game', 'Facilities', '<p>Adventure game</p>\r\n', 'maohiem.jpg', '2022-03-07 20:43:40'),
(6, 'Children area', 'Facilities', '<p>Children area</p>\r\n', 'lego.jpg', '2022-03-07 21:03:05'),
(7, 'Establish year', 'Park Infomation', '<p>2021 has been one whirlwind of a year for open of SANJALIKA.</p>\r\n', '', '2022-03-09 22:50:44'),
(8, 'Ideas of SANJALIKA', 'Park Infomation', '<p>We would like to have a new fantastic water world for everyone around the world after Covid pandemic.</p>\r\n', '', '2022-03-09 22:51:28'),
(9, 'Safe journey', 'Park Infomation', '<p>SANJALIKA water has been researched to reduce bacteria after 10 seconds.</p>\r\n', '', '2022-03-09 22:51:48'),
(10, 'New experience', 'Park Infomation', '<p>We have more than 30 games to experience to refresh your soul. We give you piece.</p>\r\n', '', '2022-03-09 22:52:04'),
(11, 'Gallery 1', 'Gallery', '<p>Gallery 1</p>\r\n', 'g1.jpg', '2022-03-09 23:02:42'),
(12, 'Gallery 2', 'Gallery', '<p>Gallery 2</p>\r\n', 'g2.jpg', '2022-03-09 23:02:59'),
(13, 'Gallery 3', 'Gallery', '<p>Gallery 3</p>\r\n', 'g3.jpg', '2022-03-09 23:03:11'),
(14, 'Gallery 4', 'Gallery', '<p>Gallery 4</p>\r\n', 'g4.jpg', '2022-03-09 23:03:24'),
(15, 'Gallery 5', 'Gallery', '<p>Gallery 5</p>\r\n', 'g5.jpg', '2022-03-09 23:03:41'),
(16, 'Gallery 6', 'Gallery', '<p>Gallery 6</p>\r\n', 'g6.jpg', '2022-03-09 23:03:55');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `Id` int(11) NOT NULL,
  `Thumbnail` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Description` text NOT NULL,
  `Price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`Id`, `Thumbnail`, `Name`, `Description`, `Price`) VALUES
(1, 't1.jpg', 'Adult Ticket', 'Height from 120cm above', 50),
(2, 't2.jpg', 'Children Ticket', 'Height below 120cm', 25),
(3, 't3.jpg', 'Family Ticket', '2 adults and 2 kids', 100);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booked_tickets`
--
ALTER TABLE `booked_tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booked_tickets`
--
ALTER TABLE `booked_tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
