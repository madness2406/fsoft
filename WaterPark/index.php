<?php
require_once("admin/dbconfig/dbconfig.php");
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SANJALIKA</title>

    <meta name="description" content="N.Agency - Responisve Landing Page for Agency">
    <meta name="keywords" content="">
    <meta name="author" content="tabthemes">

    <!-- Favicons -->
    <link rel="shortcut icon" href="img/logoo-32.png">
    <link rel="apple-touch-icon" sizes="57x57" href="img/logo-57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/logo-72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/logo-114.png">

    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <!-- CSS Files For Plugin -->
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/font-awesome/font-awesome.min.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet" />
    <link href="css/YTPlayer.css" rel="stylesheet" />
    <link href="inc/owlcarousel/css/owl.carousel.min.css" rel="stylesheet" />
    <link href="inc/owlcarousel/css/owl.theme.default.min.css" rel="stylesheet" />
    <link href="inc/revolution/css/settings.css" rel="stylesheet" />
    <link href="inc/revolution/css/layers.css" rel="stylesheet" />
    <link href="inc/revolution/css/navigation.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="homepage_slider" data-spy="scroll" data-target=".navbar-fixed-top" data-offset="100">


    <!-- Preloader -->
    <div id="preloader">
        <div id="spinner"></div>
    </div>
    <!-- End Preloader-->


    <!-- Start Navigation -->
    <header class="nav-solid" id="home">

        <nav class="navbar navbar-fixed-top">
            <div class="navigation">
                <div class="container-fluid">
                    <div class="row">

                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <!-- Logo -->
                            <div class="logo-container">
                                <div class="logo-wrap local-scroll">
                                    <a href="#home">
                                        <img class="logo" src="img/logo_final_1.png" alt="logo" data-rjs="2">
                                    </a>
                                </div>
                            </div>
                        </div> <!-- end navbar-header -->

                        <div class="col-md-8 col-xs-12 nav-wrap">
                            <div class="collapse navbar-collapse" id="navbar-collapse">

                                <ul class="nav navbar-nav navbar-right">
                                    <li><a data-scroll href="#home">Home</a></li>
                                    <li><a data-scroll href="#information">Park Information</a></li>
                                    <li><a data-scroll href="#gallery">Gallery</a></li>
                                    <li><a data-scroll href="#foodzone">Food Zone</a></li>
                                    <li><a data-scroll href="#buy_ticket">Buy ticket</a></li>
                                    <li><a data-scroll href="#facilities">Facilities</a></li>
                                    <li><a data-scroll href="#contact">Contact Us</a></li>
                                    <li><a data-scroll href="#locationMap">Location Map</a></li>
                                    <li><a data-scroll href="#aboutus">About Us</a></li>
                                </ul>

                            </div>
                        </div> <!-- /.col -->

                    </div> <!-- /.row -->
                </div>
                <!--/.container -->
            </div> <!-- /.navigation-overlay -->
        </nav> <!-- /.navbar -->

    </header>
    <!-- End Navigation -->


    <!-- Start Intro -->
    <section id="slider">
        <div class="rev_slider_wrapper fullscreen-container" data-alias="agency-website" id="rev_slider_280_1_wrapper" style="background-color:#fff;padding:0px;height:1080px;">
            <!-- START REVOLUTION SLIDER 5.1.4 fullscreen mode -->
            <div class="rev_slider fullscreenbanner" id="rev_slider_nagency" style="display:none;">
                <ul style="display:none;">

                    <!-- slider Item 1 -->
                    <li data-index="rs-1" data-transition="fadetotopfadefrombottom" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" data-rotate="0" data-saveperformance="off" data-title="N.Agency" data-description="">
                        <img src="img/slider/slide_1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption NotGeneric-Title tp-resizeme white-color rs-parallaxlevel-3" id="slide-1-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-160','-170','-170','-120']" data-fontsize="['70','60','60','36']" data-lineheight="['70','60','60','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">Welcome to SANJALIKA
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption NotGeneric-SubTitle tp-resizeme white-color rs-parallaxlevel-4" id="slide-1-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-80','-90','-90','-60']" data-fontsize="['28','24','24','20']" data-lineheight="['28','24','36','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">Let's chill out with SANJALIKA
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption rs-parallaxlevel-5" id="slide-1-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['15','5','5','10']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;" data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[175%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]' data-responsive_offset="on" data-responsive="off" style=""><a data-scroll href='#portfolio' class='btn btn-main btn-white'>View Gallery</a>
                        </div>

                    </li>

                    <!-- slider Item 2 -->
                    <li data-index="rs-3" data-transition="slideremoveright" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" data-rotate="0" data-saveperformance="off">
                        <img src="img/slider/slide_2.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption NotGeneric-Title tp-resizeme black-color rs-parallaxlevel-3" id="slide-2-layer-1" data-x="['left','left','left','left']" data-hoffset="['0','50','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-100','-85']" data-fontsize="['70','60','60','36']" data-lineheight="['70','60','60','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rx:0deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">Swimming with dolphins
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme NotGeneric-Text black-color rs-parallaxlevel-4" id="slide-2-layer-2" data-x="['left','left','left','left']" data-hoffset="['0','50','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-10']" data-fontsize="['18','18','20','14']" data-lineheight="['28','28','32','24']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6;">Get
                            nose to bottlenose with a new friend that will live in your heart forever <br> when you
                            experience the highlight of your day, a one-on-one dolphin encounter.<br> Get ready to
                            become one of the pod at the best place to swim with dolphins, SANJALIKA
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption rs-parallaxlevel-5" id="slide-2-layer-3" data-x="['left','left','left','left']" data-hoffset="['0','50','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['105','105','115','85']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;" data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[175%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]' data-responsive_offset="on" data-responsive="off" style=""><a data-scroll href='#portfolio' class='btn btn-main btn-black'>View Gallery</a>
                        </div>

                    </li>

                    <!-- slider Item 3 -->
                    <li data-index="rs-2" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" data-rotate="0" data-saveperformance="off">
                        <img alt="" class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="5000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="110" src="img/slider/slide_3.jpg">

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption NotGeneric-Title tp-resizeme white-color rs-parallaxlevel-3" id="slide-3-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-75','-70','-70','-45']" data-fontsize="['70','60','60','36']" data-lineheight="['70','60','60','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">100% Fresh Seafood
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption NotGeneric-SubTitle white-color tp-resizeme rs-parallaxlevel-2" id="slide-3-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['28','24','24','20']" data-lineheight="['28','24','36','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">50+ seafood menu made by 5 star MICHELIN Chef
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption rs-parallaxlevel-5" id="slide-3-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['90','90','100','75']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;" data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[175%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]' data-responsive_offset="on" data-responsive="off" style=""><a href='#' class='btn btn-main btn-transparent-light'>Food zone</a>
                        </div>

                    </li>

                </ul>
                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
        </div>
    </section>
    <!-- End Intro -->


    <!-- Start Service -->
    <section id="information" class="p-top-80 p-bottom-80">
        <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!-- Section Title -->
                    <div class="section-title text-center m-bottom-40">
                        <h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.6s"><strong>Park
                                information</strong></h2>
                        <div class="divider-center-small wow zoomIn" data-wow-duration="1s" data-wow-delay="0.6s"></div>
                        <p class="section-subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s"><em>Let's
                                chill out with SANJALIKA.</em></p>
                    </div>
                </div> <!-- /.col -->
            </div> <!-- /.row -->


            <div class="row">

                <!-- Service Item -->
                <?php
                $sql = "SELECT * FROM news WHERE category = 'Park Infomation' ORDER BY time DESC";
                $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result) > 0) {
                    $data = mysqli_fetch_all($result, 1);
                    for ($i = 0; $i < 4; $i++) {
                ?>
                        <div class="col-md-3 col-sm-6 m-bottom-20">
                            <div class="service wow zoomIn" data-wow-duration="1s" data-wow-delay="0.6s">
                                <h4><?php echo $data[$i]['title']; ?></h4>
                                <div class="service-text">
                                    <?php echo $data[$i]['content']; ?>
                                </div>
                            </div>
                        </div> <!-- /.col -->
                <?php
                    }
                }
                ?>
            </div> <!-- /.row -->

        </div> <!-- /.container -->
    </section>
    <!-- End Service -->


    <!-- Start Portfolio -->
    <section id="gallery" class="light-bg p-top-80 p-bottom-80">
        <div class="container">

            <!-- Section Title -->
            <div class="section-title text-center m-bottom-30">
                <h2><strong>Gallery</strong></h2>
                <div class="divider-center-small"></div>
            </div>

            <!-- Portfolio-filter -->
            <ul class="pf-filter text-center list-inline">
                <li><a href="#" class="iso-active iso-button">#SANJALIKA</a></li>
                <li><a href="#" class="iso-button">#chillout</a></li>
                <li><a href="#" class="iso-button">#hot_summer</a></li>
                <li><a href="#" class="iso-button">#machelinestar</a></li>
                <li><a href="#" class="iso-button">#dolphins</a></li>
            </ul>

            <!-- Portfolio -->
            <div class="portfolio portfolio-isotope col-3 gutter">

                <!-- Portfolio Item -->
                <?php
                $sql = "SELECT * FROM news WHERE category = 'Gallery' ORDER BY time DESC";
                $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result) > 0) {
                    $data = mysqli_fetch_all($result, 1);
                    for ($i = 0; $i < 6; $i++) {
                ?>
                        <div class="pf-item branding graphic">
                            <a href="admin/images/Gallery/<?php echo $data[$i]['picture']; ?>" class="pf-style lightbox-image mfp-image">
                                <div class="pf-image">
                                    <img src="admin/images/Gallery/<?php echo $data[$i]['picture']; ?>" alt="">
                                </div> <!-- .pf-image -->
                            </a> <!-- .pf-style -->
                        </div>
                <?php
                    }
                }
                ?>
            </div> <!-- Portfolio -->

        </div> <!-- /.container -->
    </section>
    <!-- End Portfolio -->


    <!-- Start Video Section -->
    <section class="video-bg overlay-dark" style="background-image:url(img/video-bg.jpg)">
        <div class="js-height-full container">
            <!-- video setting -->
            <div class="video-player" data-property="{videoURL:'https://youtu.be/wCt0UTA6NgQ',containment:'.video-bg',autoPlay:true, mute:true, loop:true, showControls:false, startAt:0, opacity:1}">
            </div>

            <div class="vertical-section">
                <div class="vertical-content">
                    <div class="video-caption text-center white-color">
                        <h2 class="p-top-20">Welcom to SANJALIKA</h2>
                        <div class="divider-center-small divider-white"></div>
                        <h6 class="p-bottom-20">Entertainment paradise</h6>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Video Section -->

    <!-- Start Foodzone-->
    <section id="foodzone">
        <div class="foodzone-title">
            <h2 class="wow fadeInDown animated" data-wow-duration="1s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.6s; animation-name: fadeInDown; text-align: center;">FOOD ZONE</h2>
            <div class="divider-center-small wow zoomIn animated" data-wow-duration="1s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.6s; animation-name: zoomIn;"></div>
        </div>

        <?php
                $sql = "SELECT * FROM news WHERE category = 'Food zone' ORDER BY time DESC";
                $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result) > 0) {
                    $data = mysqli_fetch_all($result, 1);
                    ?>
        <div class="foodzone-section container">
            <div class="foodzone-row">
                <div class="foodzone-img foodzone-img1">
                    <img src="admin/images/Foodzone/<?php echo $data[0]['picture']; ?>" class="img-fluid" alt="img">
                </div>
                <div class="foodzone-info">
                    <h2><?php echo $data[0]['title']; ?></h2>
                    <?php echo $data[0]['content']; ?>
                </div>
            </div>
            <div class="foodzone-row">
                <div class="foodzone-info">
                    <h2><?php echo $data[1]['title']; ?></h2>
                    <?php echo $data[1]['content']; ?>
                </div>
                <div class="foodzone-img foodzone-img2">
                    <img src="admin/images/Foodzone/<?php echo $data[1]['picture']; ?>" class="img-fluid" alt="img">
                </div>
            </div>
        </div>
        <?php
                }
                ?>
    </section>
    <!-- End Foodzone-->

    <!-- Start blog -->
    <section id="buy_ticket" class="p-top-80 p-bottom-80">

        <div class="container ">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!-- Section Title -->
                    <div class="section-title text-center m-bottom-40">
                        <h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.6s">BUY TICKET</h2>
                        <div class="divider-center-small wow zoomIn" data-wow-duration="1s" data-wow-delay="0.6s"></div>
                    </div>
                </div> <!-- /.col -->
            </div> <!-- /.row -->

            <div class="row">
                <!-- === blog === -->
                <div id="owl-blog" class="owl-carousel owl-theme">
                    <!-- Load ticket data -->
                    <?php
                    $sql =  "SELECT * FROM ticket";
                    $result = mysqli_query($conn, $sql);
                    if (mysqli_num_rows($result) > 0) {
                        $data = mysqli_fetch_all($result, 1);
                        for ($i = 0; $i < count($data); $i++) {
                    ?>
                            <div class="blog wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.7s">
                                <div class="blog-media">
                                    <a href="blog_single_post.html"><img src="admin/images/ticket/<?php echo $data[$i]['Thumbnail']; ?>" alt=""></a>
                                </div>

                                <div class="blog-post-body">
                                    <h4><a class="title" href="blog_single_post.html"><?php echo $data[$i]['Name']; ?></a></h4>
                                    <p class="p-bottom-20">
                                    <h2 style='color:green'><?php echo $data[$i]['Price']; ?>$</h2>
                                    </br><?php echo $data[$i]['Description']; ?></p>
                                </div>
                            </div> <!-- /.blog -->
                    <?php
                        }
                    }
                    ?>
                </div><!-- /#owl-testimonials -->

            </div> <!-- /.row -->

            <div class="contact-form p-bottom-30">
                <div class="row">

                    <form name="buy_ticket_form" id="contact-form" action="admin/action/buy-ticket.php" method="post">
                        <div class="row">
                            <div class="col-sm-6 ">
                                <div class="contact-form-item wow zoomIn">
                                    <input name="name" id="name" type="text" required placeholder="Your Name: *" />
                                    <span class="error" id="err-name">please enter name</span>
                                </div>

                                <div class="contact-form-item wow zoomIn">
                                    <input name="personal_id" id="personal-id" type="text" required placeholder="Personal ID: *" />
                                    <span class="error" id="err-personal-id">please enter personal id</span>
                                </div>

                                <div class="contact-form-item wow zoomIn">
                                    <input name="email" id="email" type="email" required placeholder="E-Mail: *" />
                                    <span class="error" id="err-email">please enter e-mail</span>
                                    <span class="error" id="err-emailvld">e-mail is not a valid format</span>
                                </div>

                                <div class="contact-form-item wow zoomIn">
                                    <input name="phone_number" id="phone-number" type="text" required placeholder="Phone number: *" />
                                    <span class="error" id="err-phone-number">please enter e-mail</span>
                                    <span class="error" id="err-phone-numbervld">e-mail is not a valid format</span>
                                </div>
                            </div>

                            <div class="col-sm-6 ">
                                <div class="col-sm-12 contact-form-item">
                                    <span><?php echo $data[0]['Name'] ?></span>(<span id="number-of-sww-ticket"></span>)
                                    <input type="range" min="0" max="10" value="0" class="slider" id="sww-ticket" name="t0">
                                </div>

                                <div class="col-sm-12 contact-form-item">
                                    <span><?php echo $data[1]['Name'] ?></span>(<span id="number-of-sd-ticket"></span>)
                                    <input type="range" min="0" max="10" value="0" class="slider" id="sd-ticket" name="t1">
                                </div>

                                <div class="col-sm-12 contact-form-item">
                                    <span><?php echo $data[2]['Name'] ?></span>(<span id="number-of-sb-ticket"></span>)
                                    <input type="range" min="0" max="10" value="0" class="slider" id="sb-ticket" name="t2">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="contact-form-item">
                                <button class="send_message btn btn-main btn-theme wow fadeInUp" name="buy-ticket" id="send" data-lang="en">Buy now</button>
                            </div>
                        </div>


                        <div class="row">
                            <div class="clear"></div>
                            <div class="error text-align-center" id="err-form">There was a problem validating the form please check!</div>
                            <div class="error text-align-center" id="err-timedout">The connection to the server timed out!</div>
                            <div class="error" id="err-state"></div>
                        </div>
                    </form>

                    <div class="clearfix"></div>
                    <div id="ajaxsuccess">Successfully sent!!</div>
                    <div class="clear"></div>

                </div> <!-- /.contacts-form & inner row -->
            </div> <!-- /.col -->

        </div> <!-- /.container -->

    </section>

    <!--Start Facilities-->
    <!DOCTYPE html>
    <html>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <section id="facilities"></section>
    <style>
        body {
            font-family: Arial;
            margin: 0;
        }

        * {
            box-sizing: border-box;
        }

        img {
            vertical-align: middle;
        }

        /* Position the image container (needed to position the left and right arrows) */
        .container {
            position: relative;
        }

        /* Hide the images by default */
        .mySlides {
            display: none;
        }

        .mySlides__2nd {
            width: 100%;
            height: 55rem;
        }

        .mySlides__2nd>img {
            height: 100%;
        }

        /* Add a pointer when hovering over the thumbnail images */
        .cursor {
            cursor: pointer;
        }

        /* Next & previous buttons */
        .prev,
        .next {
            cursor: pointer;
            position: absolute;
            top: 40%;
            width: auto;
            padding: 16px;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover,
        .next:hover {
            background-color: rgba(0, 0, 0, 0.8);
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* Container for image text */
        .caption-container {
            text-align: center;
            background-color: #222;
            padding: 2px 16px;
            color: white;
        }

        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Six columns side by side */
        .column {
            width: calc(100%/6);
            height: 13rem;
        }

        .column>img {
            width: 100%;
            height: 100%;
        }

        /* Add a transparency effect for thumnbail images */
        .demo {
            opacity: 0.6;
        }

        .active,
        .demo:hover {
            opacity: 1;
        }
    </style>

    <body>

        <h2 style="text-align:center">Facilities</h2>
        <div class="container">
            <?php
            $sql = "SELECT * FROM news WHERE category = 'Facilities' ORDER BY time DESC";
            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0) {
                $data = mysqli_fetch_all($result, 1);
                for ($i = 0; $i < 6; $i++) {
            ?>

                    <div class="mySlides mySlides__2nd">
                        <div class="numbertext"><?php echo $i + 1; ?> / 6</div>
                        <img src="admin/images/Facilities/<?php echo $data[$i]['picture']; ?>" style="width:100%">
                    </div>

                <?php
                }
                ?>

                <a class="prev" onclick="plusSlides(-1)">❮</a>
                <a class="next" onclick="plusSlides(1)">❯</a>

                <div class="caption-container">
                    <p id="caption"></p>
                </div>

                <div class="row" style="display: flex;width: 100%;margin: auto">
                    <?php
                    for ($i = 0; $i < 6; $i++) {
                    ?>
                        <div class="column">
                            <img class="demo cursor" src="admin/images/Facilities/<?php echo $data[$i]['picture']; ?>" style="width:100%" onclick="currentSlide(<?php echo $i + 1; ?>)" alt="<?php echo $data[$i]['title']; ?>">
                        </div>
                <?php
                    }
                }
                ?>
                </div>
        </div>

        <script>
            var slideIndex = 1;
            showSlides(slideIndex);

            function plusSlides(n) {
                showSlides(slideIndex += n);
            }

            function currentSlide(n) {
                showSlides(slideIndex = n);
            }

            function showSlides(n) {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("demo");
                var captionText = document.getElementById("caption");
                if (n > slides.length) {
                    slideIndex = 1
                }
                if (n < 1) {
                    slideIndex = slides.length
                }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
                captionText.innerHTML = dots[slideIndex - 1].alt;
            }
        </script>

    </body>

    </html>

    <!--End Facilities-->

    <!-- Start Contact -->
    <section id="contact" class="p-top-80 p-bottom-50">
        <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!-- Section Title -->
                    <div class="section-title text-center m-bottom-40">
                        <h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.6s">Contact</h2>
                        <div class="divider-center-small wow zoomIn" data-wow-duration="1s" data-wow-delay="0.6s"></div>
                        <p class="section-subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s"><em>Lose
                                away off why half led have near bed. At engage simple father of period others except. My
                                giving do summer time dance hero of though narrow marked at.</em></p>
                    </div>
                </div> <!-- /.col -->
            </div> <!-- /.row -->

            <div class="row">

                <!-- === Contact Form === -->
                <div class="col-md-7 col-sm-7 p-bottom-30">
                    <div class="contact-form row">

                        <form name="ajax-form" id="ajax-form" action="admin/action/contact.php" method="post">
                            <div class="col-sm-6 contact-form-item wow zoomIn">
                                <input name="name" id="name" type="text" placeholder="Your Name: *" />
                                <span class="error" id="err-name">please enter name</span>
                            </div>
                            <div class="col-sm-6 contact-form-item wow zoomIn">
                                <input name="email" id="email" type="text" placeholder="E-Mail: *" />
                                <span class="error" id="err-email">please enter e-mail</span>
                                <span class="error" id="err-emailvld">e-mail is not a valid format</span>
                            </div>
                            <div class="col-sm-12 contact-form-item wow zoomIn">
                                <textarea name="message" id="message" placeholder="Your Message"></textarea>
                            </div>
                            <div class="col-sm-12 contact-form-item">
                                <button class="send_message btn btn-main btn-theme wow fadeInUp" id="send" data-lang="en">submit</button>
                            </div>
                            <div class="clear"></div>
                            <div class="error text-align-center" id="err-form">There was a problem validating the form
                                please check!</div>
                            <div class="error text-align-center" id="err-timedout">The connection to the server timed
                                out!</div>
                            <div class="error" id="err-state"></div>
                        </form>

                        <div class="clearfix"></div>
                        <div id="ajaxsuccess">Successfully sent!!</div>
                        <div class="clear"></div>

                    </div> <!-- /.contacts-form & inner row -->
                </div> <!-- /.col -->

                <!-- === Contact Information === -->
                <div class="col-md-5 col-sm-5 p-bottom-30">
                    <address class="contact-info">

                        <p class="m-bottom-30 wow slideInRight">Spring formal no county ye waited. My whether cheered at
                            regular it of promise blushes perhaps.</p>

                        <!-- === Location === -->
                        <div class="m-top-20 wow slideInRight">
                            <div class="contact-info-icon">
                                <i class="fa fa-location-arrow"></i>
                            </div>
                            <div class="contact-info-title">
                                Address:
                            </div>
                            <div class="contact-info-text">
                                57 Le Thanh Nghi, Bach Khoa, Hai Ba Trung, Ha Noi
                            </div>
                        </div>

                        <!-- === Phone === -->
                        <div class="m-top-20 wow slideInRight">
                            <div class="contact-info-icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="contact-info-title">
                                Phone number:
                            </div>
                            <div class="contact-info-text">
                                +1-000-1111-3333
                            </div>
                        </div>

                        <!-- === Mail === -->
                        <div class="m-top-20 wow slideInRight">
                            <div class="contact-info-icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="contact-info-title">
                                Email:
                            </div>
                            <div class="contact-info-text">
                                support@eProject1.aptech
                            </div>
                        </div>

                    </address>
                </div> <!-- /.col -->

            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>
    <!-- End Contact -->

    <!--Location Map-->

    <meta charset="utf-8">
    <title>Customize the scroll-bar</title>
    <header class="nav-solid" id="locationMap">
        </head>

        <body>
            <center>
                <h1 style="color:lawngreen;">
                    Location Map
                </h1>

                <div>
                    <!-- Google Map Copied Code -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.7295561235915!2d105.84769451473096!3d21.003475286012016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ad58455db2ab%3A0x9b3550bc22fd8bb!2zNTQgUC4gTMOqIFRoYW5oIE5naOG7iywgQsOhY2ggS2hvYSwgSGFpIELDoCBUcsawbmcsIEjDoCBO4buZaSwgVmlldG5hbQ!5e0!3m2!1sfr!2s!4v1645668879143!5m2!1sfr!2s" width="1000" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0">
                    </iframe>
                </div>
            </center>
        </body>


        <!-- Start Team -->
        <section id="aboutus" class="p-top-80 p-bottom-50">
            <div class="container">

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <!-- Section Title -->
                        <div class="section-title text-center m-bottom-40">
                            <h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.6s">Team Members</h2>
                            <div class="divider-center-small wow zoomIn" data-wow-duration="1s" data-wow-delay="0.6s"></div>
                            <p class="section-subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s"><em>Coming
                                    together, sharing together, working together, succeeding together</em></p>
                        </div>
                    </div> <!-- /.col -->
                </div> <!-- /.row -->

                <div class="row">

                    <!-- === Team Item 1 === -->
                    <div class="col-md-3 col-sm-6 col-xs-6 p-bottom-30">
                        <div class="team-item wow zoomIn" data-wow-duration="1s" data-wow-delay="0.9s">
                            <div class="team-item-image">
                                <div class="team-item-image-overlay">
                                    <div class="team-item-icons">
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                                <img src="img/team/1.jpg" alt="" />
                            </div>
                            <div class="team-item-info">
                                <div class="team-item-name">
                                    Tuan Nguyen Tran
                                </div>
                                <div class="team-item-position">
                                    Student1285912
                                </div>
                            </div>
                        </div>
                    </div> <!-- /.col -->

                    <!-- === Team Item 2 === -->
                    <div class="col-md-3 col-sm-6 col-xs-6 p-bottom-30">
                        <div class="team-item wow zoomIn" data-wow-duration="1s" data-wow-delay="0.9s">
                            <div class="team-item-image">
                                <div class="team-item-image-overlay">
                                    <div class="team-item-icons">
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                                <img src="img/team/2.jpg" alt="" />
                            </div>
                            <div class="team-item-info">
                                <div class="team-item-name">
                                    Trang Nguyen Thi Huyen
                                </div>
                                <div class="team-item-position">
                                    Student1312776
                                </div>
                            </div>
                        </div>
                    </div> <!-- /.col -->

                    <!-- === Team Item 3 === -->
                    <div class="col-md-3 col-sm-6 col-xs-6 p-bottom-30">
                        <div class="team-item wow zoomIn" data-wow-duration="1s" data-wow-delay="0.9s">
                            <div class="team-item-image">
                                <div class="team-item-image-overlay">
                                    <div class="team-item-icons">
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                                <img src="img/team/3.jpg" alt="" />
                            </div>
                            <div class="team-item-info">
                                <div class="team-item-name">
                                    Minh Phan Bao
                                </div>
                                <div class="team-item-position">
                                    Student1269221
                                </div>
                            </div>
                        </div>
                    </div> <!-- /.col -->

                    <!-- === Team Item 4 === -->
                    <div class="col-md-3 col-sm-6 col-xs-6 p-bottom-30">
                        <div class="team-item wow zoomIn" data-wow-duration="1s" data-wow-delay="0.9s">
                            <div class="team-item-image">
                                <div class="team-item-image-overlay">
                                    <div class="team-item-icons">
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                                <img src="img/team/4.jpg" alt="" />
                            </div> <!-- /.team-item-image -->
                            <div class="team-item-info">
                                <div class="team-item-name">
                                    Thanh Nguyen Trung
                                </div>
                                <div class="team-item-position">
                                    Student1275488
                                </div>
                            </div> <!-- /.team-item-info -->
                        </div> <!-- /.team-item -->
                    </div> <!-- /.col -->

                </div> <!-- /.row -->

            </div> <!-- /.container -->
        </section>
        <!-- End Team -->
        <!-- Start Footer -->
        <footer class="site-footer">
            <div class="container">
                <small class="copyright pull-left">Copyrights © 2019 All Rights Reserved By <a href="http://www.tabthemes.com/">tabthemes</a>.</small>
                <div class="social-icon pull-right">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-pinterest"></i></a>
                    <a href="#"><i class="fa fa-google"></i></a>
                    <a href="#"><i class="fa fa-rss"></i></a>
                    <a href="#"><i class="fa fa-globe"></i></a>
                </div>
                <!-- /social-icon -->
            </div>
            <!-- /container -->
        </footer>
        <!-- End Footer -->


        <!-- Back to top -->
        <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>
        <!-- /Back to top -->


        <!-- jQuery -->
        <script src="js/jquery.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <!-- Components Plugin -->
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/smooth-scroll.js"></script>
        <script src="js/jquery.appear.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/isotope.pkgd.min.js"></script>
        <script src="js/jquery.mb.YTPlayer.js"></script>
        <script src="js/retina.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="inc/owlcarousel/js/owl.carousel.min.js"></script>
        <script src="inc/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="inc/revolution/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Contact Form -->
        <!-- <script src="js/contact.js"></script> -->

        <!-- Custom Plugin -->
        <script src="js/custom.js"></script>

        <!-- RS Plugin Extensions -->
        <script src="inc/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="inc/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="inc/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="inc/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="inc/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="inc/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="inc/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="inc/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="inc/revolution/js/extensions/revolution.extension.parallax.min.js"></script>

        <!-- Load data -->
        <script src="js/loaddata.js"></script>

        <script>
            var tpj = jQuery;

            var revapi280;
            tpj(document).ready(function() {
                if (tpj("#rev_slider_nagency").revolution == undefined) {
                    revslider_showDoubleJqueryError("#rev_slider_nagency");
                } else {
                    revapi280 = tpj("#rev_slider_nagency").show().revolution({
                        sliderType: "standard",
                        sliderLayout: "fullscreen",
                        dottedOverlay: "none",
                        delay: 90000,
                        navigation: {
                            keyboardNavigation: "off",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            onHoverStop: "off",
                            touch: {
                                touchenabled: "on",
                                swipe_threshold: 75,
                                swipe_min_touches: 1,
                                swipe_direction: "horizontal",
                                drag_block_vertical: false
                            },
                            arrows: {
                                style: "uranus",
                                enable: true,
                                hide_onmobile: true,
                                hide_under: 496,
                                hide_onleave: true,
                                hide_delay: 200,
                                hide_delay_mobile: 1200,
                                tmp: '',
                                left: {
                                    h_align: "left",
                                    v_align: "center",
                                    h_offset: 20,
                                    v_offset: 0
                                },
                                right: {
                                    h_align: "right",
                                    v_align: "center",
                                    h_offset: 20,
                                    v_offset: 0
                                }
                            }
                        },
                        responsiveLevels: [1200, 991, 767, 480],
                        visibilityLevels: [1200, 991, 767, 480],
                        gridwidth: [1200, 991, 767, 480],
                        gridheight: [868, 768, 960, 720],
                        lazyType: "none",
                        parallax: {
                            type: "mouse+scroll",
                            origo: "slidercenter",
                            speed: 2000,
                            levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
                            disable_onmobile: "on"
                        },
                        shadow: 0,
                        spinner: "spinner2",
                        autoHeight: "off",
                        fullScreenAutoWidth: "off",
                        fullScreenAlignForce: "off",
                        fullScreenOffsetContainer: "",
                        fullScreenOffset: "0",
                        disableProgressBar: "on",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            disableFocusListener: false,
                        }
                    });
                }
            }); /*ready*/
        </script>

</body>

</html>