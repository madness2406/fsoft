﻿using ParkyAPI.Data;
using ParkyAPI.Models;
using ParkyAPI.Repositories.IRepositories;

namespace ParkyAPI.Repositories
{
    public class NationalParkRepository : INationalParkRepository
    {
        private readonly ApplicationDbContext _db;
        public NationalParkRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public bool CreateNationalPark(NationalPark park)
        {
            _db.NationalParks.Add(park);
            return Save();
        }

        public bool DeleteNationalPark(NationalPark park)
        {
            _db.NationalParks.Remove(park);
            return Save();
        }

        public NationalPark GetNationalPark(int id)
        {
            return _db.NationalParks.FirstOrDefault(n => n.Id == id);
        }

        public ICollection<NationalPark> GetNationalParks()
        {
            return _db.NationalParks.OrderBy(n => n.Name).ToList();
        }

        public bool NationParkExists(string name)
        {
            return _db.NationalParks.Any(o => o.Name.ToLower().Trim() == name.ToLower().Trim());
        }

        public bool NationParkExists(int id)
        {
            return _db.NationalParks.Any(o => o.Id == id);
        }

        public bool Save()
        {
            return _db.SaveChanges() >= 0 ? true : false;
        }

        public bool UpdateNationalPark(NationalPark park)
        {
            _db.NationalParks.Update(park);
            return Save();
        }
    }
}
