﻿using Microsoft.EntityFrameworkCore;
using ParkyAPI.Data;
using ParkyAPI.Models;
using ParkyAPI.Repositories.IRepositories;

namespace ParkyAPI.Repositories
{
    public class TrailRepository : ITrailRepository
    {
        private readonly ApplicationDbContext _db;
        public TrailRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public bool CreateTrail(Trail park)
        {
            _db.Trails.Add(park);
            return Save();
        }

        public bool DeleteTrail(Trail park)
        {
            _db.Trails.Remove(park);
            return Save();
        }

        public Trail GetTrail(int id)
        {
            return _db.Trails.Include(c => c.NationalPark).FirstOrDefault(n => n.Id == id);
        }

        public ICollection<Trail> GetTrails()
        {
            return _db.Trails.Include(c => c.NationalPark).OrderBy(n => n.Name).ToList();
        }

        public bool TrailExists(string name)
        {
            return _db.Trails.Any(o => o.Name.ToLower().Trim() == name.ToLower().Trim());
        }

        public bool TrailExists(int id)
        {
            return _db.Trails.Any(o => o.Id == id);
        }

        public bool Save()
        {
            return _db.SaveChanges() >= 0 ? true : false;
        }

        public bool UpdateTrail(Trail park)
        {
            _db.Trails.Update(park);
            return Save();
        }

        public ICollection<Trail> GetTrailsInNationalPark(int id)
        {
            return _db.Trails.Include(c => c.NationalPark).Where(c => c.NationalParkId == id).ToList();
        }
    }
}
