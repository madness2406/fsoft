﻿using ParkyAPI.Models;

namespace ParkyAPI.Repositories.IRepositories
{
    public interface ITrailRepository
    {
        ICollection<Trail> GetTrails();
        ICollection<Trail> GetTrailsInNationalPark(int id);
        Trail GetTrail(int id);
        bool TrailExists(string name);
        bool TrailExists(int id);
        bool CreateTrail(Trail park);
        bool UpdateTrail(Trail park);
        bool DeleteTrail(Trail park);
        bool Save();
    }
}
