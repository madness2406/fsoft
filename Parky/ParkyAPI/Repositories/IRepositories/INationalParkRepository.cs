﻿using ParkyAPI.Models;

namespace ParkyAPI.Repositories.IRepositories
{
    public interface INationalParkRepository
    {
        ICollection<NationalPark> GetNationalParks();
        NationalPark GetNationalPark(int id);
        bool NationParkExists(string name);
        bool NationParkExists(int id);
        bool CreateNationalPark(NationalPark park);
        bool UpdateNationalPark(NationalPark park);
        bool DeleteNationalPark(NationalPark park);
        bool Save();
    }
}
