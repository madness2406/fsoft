﻿namespace ParkyWeb.Models.ViewModels
{
    public class IndexVM
    {
        //[ValidateNever]
        public IEnumerable<NationalPark> NationalParkList { get; set; }
        public IEnumerable<Trail> TrailList { get; set; }
    }
}
