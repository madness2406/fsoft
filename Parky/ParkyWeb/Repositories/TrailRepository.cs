﻿using ParkyWeb.Models;
using ParkyWeb.Repositories.IRepositories;

namespace ParkyWeb.Repositories
{
    public class TrailRepository : Repository<Trail>, ITrailRepository
    {
        private readonly IHttpClientFactory _clientFactory;
        public TrailRepository(IHttpClientFactory clientFactory) : base(clientFactory)
        {
            _clientFactory = clientFactory;
        }
    }
}
