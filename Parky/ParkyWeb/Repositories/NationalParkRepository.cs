﻿using ParkyWeb.Models;
using ParkyWeb.Repositories.IRepositories;

namespace ParkyWeb.Repositories
{
    public class NationalParkRepository : Repository<NationalPark>, INationalParkRepository
    {
        private readonly IHttpClientFactory _clientFactory;
        public NationalParkRepository(IHttpClientFactory clientFactory) : base(clientFactory)
        {
            _clientFactory = clientFactory;
        }
    }
}
