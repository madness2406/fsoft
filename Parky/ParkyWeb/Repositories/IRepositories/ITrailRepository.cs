﻿using ParkyWeb.Models;

namespace ParkyWeb.Repositories.IRepositories
{
    public interface ITrailRepository : IRepository<Trail>
    {
    }
}
