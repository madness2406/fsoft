﻿using ParkyWeb.Models;

namespace ParkyWeb.Repositories.IRepositories
{
    public interface INationalParkRepository : IRepository<NationalPark>
    {
    }
}
