﻿using FA.JustBlog.Core.IRepository;

namespace FA.JustBlog.Core.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository Category { get; }
        IPostRepository Post { get; }
        ITagRepository Tag { get; }
        IPostTagMapRepository PostTagMap { get; }
        ICommentRepository Comment { get; }
        int Save();
    }
}
