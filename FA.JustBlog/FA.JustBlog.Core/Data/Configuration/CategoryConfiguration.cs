﻿
using FA.JustBlog.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA.JustBlog.Core.Data.Configuration
{
    /// <summary>
    /// Config properti for Categorys table
    /// </summary>
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name).IsRequired().HasMaxLength(255);
            builder.Property(c => c.UrlSlug).HasMaxLength(255);
            builder.HasIndex(c => c.UrlSlug).IsUnique();
            builder.Property(c => c.Description).HasMaxLength(1024);

            //Add seed data
            builder.HasData(
                new Category()
                {
                    Id = 1,
                    Name = "Category 1",
                    Description = "This is description 1",
                },
                new Category()
                {
                    Id = 2,
                    Name = "Category 2",
                    Description = "This is description 2",
                },
                new Category()
                {
                    Id = 3,
                    Name = "Category 3",
                    Description = "This is description 3",
                }
            );
        }
    }
}
