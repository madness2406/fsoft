﻿using FA.JustBlog.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA.JustBlog.Core.Data.Configuration
{
    public class PostTagMapConfiguration : IEntityTypeConfiguration<PostTagMap>
    {
        public void Configure(EntityTypeBuilder<PostTagMap> builder)
        {
            //Create composite primakey for PostTagMap table
            builder.HasKey(p => new { p.PostId, p.TagId });

            //Create many to many relation between Tag table and Post table 
            builder.HasOne(p => p.Post).WithMany(p => p.PostTagMaps).HasForeignKey(p => p.PostId);
            builder.HasOne(p => p.Tag).WithMany(p => p.PostTagMaps).HasForeignKey(p => p.TagId);

            //Add seed data
            builder.HasData(
                new PostTagMap()
                {
                    PostId = 1,
                    TagId = 1
                },
                new PostTagMap()
                {
                    PostId = 3,
                    TagId = 1
                },
                new PostTagMap()
                {
                    PostId = 2,
                    TagId = 2
                }
            );


        }
    }
}
