﻿using FA.JustBlog.Core.Data.Configuration;
using FA.JustBlog.Core.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FA.JustBlog.Core.Data
{

    public class JustBlogContext : IdentityDbContext
    {
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<PostTagMap> postTagMaps { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public JustBlogContext(DbContextOptions<JustBlogContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //Set up for categorys table
            modelBuilder.ApplyConfiguration<Category>(new CategoryConfiguration());

            //set up for Posts table
            modelBuilder.ApplyConfiguration<Post>(new PostConfiguration());

            //Set up for Tags Table
            modelBuilder.ApplyConfiguration<Tag>(new TagConfiguration());

            //Set up for PostTagMap table
            modelBuilder.ApplyConfiguration<PostTagMap>(new PostTagMapConfiguration());

            //Set up for comments table
            modelBuilder.ApplyConfiguration<Comment>(new CommendConfiguration());

        }

    }
}
