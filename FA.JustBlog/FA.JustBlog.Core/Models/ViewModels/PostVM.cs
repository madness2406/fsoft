﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FA.JustBlog.Core.Models.ViewModels
{
    public class PostVM
    {
        [ValidateNever]
        public Post Post { get; set; }

        [ValidateNever]
        public Category Category { get; set; }

        [ValidateNever]
        public IEnumerable<SelectListItem> CategoryList { get; set; }

        [ValidateNever]
        public List<Tag> TagList { get; set; }
        public List<string> SelectedTag { get; set; }

        [ValidateNever]
        public string TimeSpan { get; set; }

        [ValidateNever]
        public string Published { get; set; }

        
    }
}
