﻿using FA.JustBlog.Core.Utility;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Core.Models
{
    public class Category
    {

        public int Id { get; set; }
        public string Name { get; set; }
        [ValidateNever]
        public string UrlSlug 
        {
            get
            {
                return Helper.FrientlyUrl(Name);
            }
            set { } 
        }
        public string Description { get; set; }
        [ValidateNever]
        public virtual IList<Post>? Posts { get; set; }
    }
}