﻿using FA.JustBlog.Core.Utility;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA.JustBlog.Core.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string? ShortDescription { get; set; }
        public string PostContent { get; set; }
        public string UrlSlug
        {
            get
            {
                return Helper.FrientlyUrl(Title);
            }
            set { }
        }
        public bool Published { get; set; } = false;
        public DateTime PostedOn { get; set; } = DateTime.Now;
        public DateTime Modified { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public virtual IList<PostTagMap> PostTagMaps { get; set; }

        //Part 8 add new fields
        public int ViewCount { get; set; }
        public int RateCount { get; set; }
        public int TotalRate { get; set; } 

        [NotMapped]
        public decimal Rate => (RateCount > 0) ? (decimal)TotalRate / RateCount : 0;
        public List<Comment> Comments { get; set; }
    }
}