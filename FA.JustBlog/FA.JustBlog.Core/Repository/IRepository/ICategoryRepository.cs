﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository;


namespace FA.JustBlog.Core.IRepository
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
        void Update(Category category);
    }
}
