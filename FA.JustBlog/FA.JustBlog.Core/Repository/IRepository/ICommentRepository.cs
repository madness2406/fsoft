﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository;


namespace FA.JustBlog.Core.IRepository
{
    public interface ICommentRepository : IGenericRepository<Comment>
    {
        void AddComment(int postId, string commentName, string commentEmail, string commentTitle, string commentBody);
        IList<Comment> GetCommentsForPost(int postId);
        IList<Comment> GetCommentsForPost(Post post);
        void Update(Comment comment); 
    }
}