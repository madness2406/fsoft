﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Utility
{
    public static class SD
    {
        public const string Role_Blog_Owner = "Admin";
        public const string Role_Contributor = "Contributor";
        public const string Role_User = "User";

    }
}
