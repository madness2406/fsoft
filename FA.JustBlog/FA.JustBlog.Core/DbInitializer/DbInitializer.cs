﻿using FA.JustBlog.Core.Data;
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Utility;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;


namespace JustBlog.Core.Data.DbInitializer
{
    public class DbInitializer : IDbInitializer
    {
        private readonly UserManager<IdentityUser> _userManager;
        // add role manager
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly JustBlogContext _db;

        public DbInitializer(
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            JustBlogContext db)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _db = db;
        }
        public void Initialize()
        {
            // migrations if they are not applied
            try
            {
                if(_db.Database.GetPendingMigrations().Count() > 0)
                {
                    _db.Database.Migrate();
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            // create roles if they are not created
            if (!_roleManager.RoleExistsAsync(SD.Role_Blog_Owner).GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole(SD.Role_Blog_Owner)).GetAwaiter().GetResult();
                _roleManager.CreateAsync(new IdentityRole(SD.Role_User)).GetAwaiter().GetResult();
                _roleManager.CreateAsync(new IdentityRole(SD.Role_Contributor)).GetAwaiter().GetResult();
                // if roles are not created, then we will create admin user as well
                _userManager.CreateAsync(new ApplicationUser
                {
                    UserName = "admin@fpt.com",
                    Email = "admin@fpt.com",
                    Name = "Admin",
                    Age = 23,
                    Address = "VN"
      
                }, "@Tuan123").GetAwaiter().GetResult();

                _userManager.CreateAsync(new ApplicationUser
                {
                    UserName = "user@fpt.com",
                    Email = "user@fpt.com",
                    Name = "User",
                    Age = 24,
                    Address = "VN"
                }, "@Tuan123").GetAwaiter().GetResult();

                _userManager.CreateAsync(new ApplicationUser
                {
                    UserName = "contributor@fpt.com",
                    Email = "contributor@fpt.com",
                    Name = "Contributor",
                    Age = 20,
                    Address = "VN"
                }, "@Tuan123").GetAwaiter().GetResult();

                ApplicationUser admin = _db.ApplicationUsers.FirstOrDefault(x => x.Email == "admin@fpt.com"); 
                ApplicationUser user = _db.ApplicationUsers.FirstOrDefault(x => x.Email == "user@fpt.com");
                ApplicationUser contributor = _db.ApplicationUsers.FirstOrDefault(x => x.Email == "contributor@fpt.com");

                _userManager.AddToRoleAsync(admin, SD.Role_Blog_Owner).GetAwaiter().GetResult();
                _userManager.AddToRoleAsync(user, SD.Role_User).GetAwaiter().GetResult();
                _userManager.AddToRoleAsync(contributor, SD.Role_Contributor).GetAwaiter().GetResult();
            }
            return;
        }
    }
}
