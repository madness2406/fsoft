﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Models.ViewModels;
using FA.JustBlog.Core.Repository;
using FA.JustBlog.Core.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Contributor")]
    public class CategoryController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public CategoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IActionResult Index()
        {
            var categoryList = _unitOfWork.Category.GetAll();

            return View(categoryList);
        }

        //Create
        //Get
        public IActionResult Create()
        {
            return View();
        }

        //Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Category obj)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.Category.Add(obj);
                _unitOfWork.Save();

                return RedirectToAction("Index");
            }
            return View();
        }

        //Update
        //Get
        public IActionResult Update(int? id)
        {
            if (id == null || id == 0)      
                return NotFound();

            var categoryFromDb = _unitOfWork.Category.GetFirstOrDefault(c => c.Id == id);

            if (categoryFromDb == null)
                return NotFound();

            return View(categoryFromDb);
        }

        //Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(Category obj)
        {
            if (ModelState.IsValid)
            {
                
                _unitOfWork.Category.Update(obj);

                _unitOfWork.Save();

                return RedirectToAction("Index");
            }
            return View(obj);
        }

        [Authorize(Roles = SD.Role_Blog_Owner)]
        public IActionResult Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var post = _unitOfWork.Post.GetFirstOrDefault(p => p.Id == id);
            _unitOfWork.Post.Delete(post);

            _unitOfWork.Save();
            return RedirectToAction("Index");
        }
    }
}
