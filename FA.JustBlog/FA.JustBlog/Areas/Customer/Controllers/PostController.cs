﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Models.ViewModels;
using FA.JustBlog.Core.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using FA.JustBlog.Core.Utility;

namespace FA.JustBlog.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class PostController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        IEnumerable<Post> postList;
        public PostController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            IList<PostVM> postVMs = new List<PostVM>();

            postList = _unitOfWork.Post.GetAll();
            foreach (var post in postList)
            {
                postVMs.Add(new PostVM()
                {
                    Post = post,
                    TimeSpan = Helper.TimeCompare(post.PostedOn)
                });
            }


            return View(postVMs);
        }


        [HttpPost]
        public IActionResult Detail(int id)
        {
            var post = _unitOfWork.Post.GetFirstOrDefault(p => p.Id == id);

            var postVm = new PostVM()
            {
                Post = post,
                TagList = _unitOfWork.Tag.GetTagByPost(post.Id).ToList(),
                Category = _unitOfWork.Category.GetFirstOrDefault(c => c.Id == post.CategoryId)
            };

            return View(postVm);
        }

        //[Route("Post/{year}/{month}/{title}")]
        public IActionResult Detail(int year, int month, string urlSlug)
        {
            var post = _unitOfWork.Post.Find(year, month, urlSlug);

            if (post == null)
                return NotFound();
            //Incre View
            post.ViewCount++;
            _unitOfWork.Post.Update(post);
            _unitOfWork.Save();


            var postVm = new PostVM()
            {
                Post = post,
                TagList = _unitOfWork.Tag.GetTagByPost(post.Id).ToList(),
                Category = _unitOfWork.Category.GetFirstOrDefault(c => c.Id == post.CategoryId)

            };
            return View(postVm);
        }

        public IActionResult LastestPost()
        {
            postList = _unitOfWork.Post.GetAll().OrderByDescending(p => p.PostedOn);

            return View(postList);
        }

        /// <summary>
        /// Get top 5 view post
        /// </summary>
        /// <param name="postList"></param>
        /// <returns></returns>
        public IActionResult MostViewedPosts(IEnumerable<Post> postList)
        {
            postList.OrderByDescending(p => p.PostedOn).Take(5).ToList();
            return View("_ListPost", postList);
        }



        public IActionResult GetPostByCategoryId(int id)
        {
            postList = _unitOfWork.Post.GetAll(p => p.CategoryId == id);
            ViewBag.CategoryName = _unitOfWork.Category.GetFirstOrDefault(c => c.Id == id).Name;
            return View(postList);
        }

        public IActionResult GetPostByTagId(int id)
        {
            var tag = _unitOfWork.Tag.GetFirstOrDefault(c => c.Id == id);
            postList = _unitOfWork.Post.GetPostsByTag(tag.Name);
            ViewBag.tagName = tag.Name;
            return View(postList);
        }

        
    }
}
